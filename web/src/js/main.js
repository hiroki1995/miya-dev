import UIkit from 'uikit';
import Icons from 'uikit/dist/js/uikit-icons';
import 'uikit/dist/css/uikit.min.css';
import '../css/index.css';
import '../css/info.css';

// loads the Icon plugin
UIkit.use(Icons);

document.getElementById('button').onclick = () => {
  location.href = '/info.html';
};
