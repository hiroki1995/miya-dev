const webpack = require('webpack');
const path = require('path');
const fs = require('fs');
const hash = require('hash-sum');

const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ImageminWebpackPlugin = require('imagemin-webpack-plugin').default;
const OptimizeCssAssetsWebpackPlugin = require('optimize-css-assets-webpack-plugin');
const TerserWebpackPlugin = require('terser-webpack-plugin');
const StylelintWebpackPlugin = require('stylelint-webpack-plugin');
const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');

const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = env => {
  const isProduction = env && env.production;
  const isClean = env && env.clean;
  const isAnalyze = env && env.analyze;

  const cacheIdentifier = hash([
    require('cache-loader/package.json').version,
    fs.readFileSync('./webpack.config.js', 'utf-8'),
    process.env.isProduction,
  ]);

  return {
    devtool: isProduction ? false : 'inline-source-map',
    mode: isProduction ? 'production' : 'development',
    entry: ['@babel/polyfill', './src/js/main.js'],
    output: {
      filename: 'js/[name].bundle.js',
      path: path.resolve(__dirname, 'dist'),
      publicPath: '/',
    },
    resolve: {
      alias: {
        '@': path.join(__dirname, 'src/js/'),
        config: path.join(__dirname, 'src/config/'),
      },
      modules: [path.resolve('./node_modules'), path.resolve('./src')],
      extensions: ['.js', '.vue'],
    },
    optimization: {
      splitChunks: {
        chunks: 'all',
        maxSize: 1000000,
        maxInitialRequests: 5,
        cacheGroups: {
          vendors: {
            test: /[\\/]node_modules[\\/]/,
            name: 'vendor',
          },
        },
      },
      minimizer: [
        new TerserWebpackPlugin({
          cache: true,
          parallel: true,
          sourceMap: true,
          terserOptions: {
            compress: {
              drop_console: true,
            },
          },
        }),
        new OptimizeCssAssetsWebpackPlugin({
          cssProcessorPluginOptions: {
            preset: [
              'default',
              {
                discardComments: {
                  removeAll: true,
                },
              },
            ],
          },
        }),
      ],
    },
    module: {
      rules: [
        {
          enforce: 'pre',
          test: /\.(js|vue)$/,
          exclude: /node_modules/,
          loader: 'eslint-loader',
          options: {
            cache: true,
            cacheIdentifier,
          },
        },
        {
          enforce: 'pre',
          test: /\.(html|vue)$/,
          exclude: /node_modules/,
          loader: 'htmlhint-loader',
        },
        {
          test: /\.(js)$/,
          use: [
            {
              loader: 'cache-loader',
              options: {
                cacheDirectory: path.join(__dirname, 'node_modules/.cache/babel-loader'),
                cacheIdentifier,
              },
            },
            {
              loader: 'babel-loader',
              options: {
                presets: [['@babel/preset-env', { modules: false }]],
                plugins: ['@babel/plugin-syntax-dynamic-import'],
              },
            },
          ],
        },
        {
          test: /\.vue$/,
          use: [
            {
              loader: 'cache-loader',
              options: {
                cacheDirectory: path.join(__dirname, 'node_modules/.cache/vue-loader'),
                cacheIdentifier,
              },
            },
            'vue-loader',
          ],
        },
        {
          test: /\.(html)$/,
          use: ['html-loader'],
        },
        {
          test: /\.(s[ac]ss|css)$/,
          use: [
            {
              loader: 'vue-style-loader',
            },
            {
              loader: 'cache-loader',
              options: {
                cacheDirectory: path.join(__dirname, 'node_modules/.cache/css-loader'),
                cacheIdentifier,
              },
            },
            {
              loader: 'css-loader',
              options: {
                sourceMap: false,
              },
            },
            {
              loader: 'postcss-loader',
              options: {
                plugins: () => [
                  require('autoprefixer')({
                    grid: true,
                  }),
                  require('postcss-preset-env')(),
                ],
              },
            },
            'sass-loader',
          ],
        },
        {
          test: /\.(jpe?g|png|gif|ico)$/,
          exclude: [path.join(__dirname, 'assets_dev')],
          use: [
            {
              loader: 'url-loader',
              options: {
                fallback: 'file-loader?outputPath=images',
                limit: 2048,
                name(file) {
                  let imgPath = file.replace(/([\\\/])[^\\\/]*?$/, '/').replace(/^.*images[\\\/]/, '');
                  return imgPath + '[name].[ext]';
                },
              },
            },
          ],
        },
      ],
    },
    plugins: [
      ...(isAnalyze ? [new BundleAnalyzerPlugin()] : []),
      ...(isClean ? [new CleanWebpackPlugin({})] : []),
      ...(isProduction
        ? [
            new ImageminWebpackPlugin({
              test: /\.png$/,
              pngquant: {
                quality: '70-80',
              },
            }),
          ]
        : []),
      new HtmlWebpackPlugin({
        filename: 'index.html',
        template: './src/html/index.html',
        inject: 'head',
        minify: {
          collapseBooleanAttributes: true,
          collapseWhitespace: true,
          includeAutoGeneratedTags: false,
          removeAttributeQuotes: true,
          removeComments: true,
          removeEmptyAttributes: true,
          removeRedundantAttributes: true,
          removeScriptTypeAttributes: true,
          removeStyleLinkTypeAttributes: true,
          sortAttributes: true,
          sortClassName: true,
          trimCustomFragments: true,
          useShortDoctype: true,
        },
      }),
      new HtmlWebpackPlugin({
        filename: 'info.html',
        template: './src/html/info.html',
        inject: 'head',
        minify: {
          collapseBooleanAttributes: true,
          collapseWhitespace: true,
          includeAutoGeneratedTags: false,
          removeAttributeQuotes: true,
          removeComments: true,
          removeEmptyAttributes: true,
          removeRedundantAttributes: true,
          removeScriptTypeAttributes: true,
          removeStyleLinkTypeAttributes: true,
          sortAttributes: true,
          sortClassName: true,
          trimCustomFragments: true,
          useShortDoctype: true,
        },
      }),
      new ScriptExtHtmlWebpackPlugin({
        defaultAttribute: 'defer',
      }),
      new CopyWebpackPlugin([{ from: './src/images/', to: 'images/' }]),
      new StylelintWebpackPlugin({
        files: ['src/css/**/*', 'src/js/**/*.vue'],
      }),
      new VueLoaderPlugin(),
      new webpack.optimize.MinChunkSizePlugin({
        minChunkSize: 32768,
      }),
      new webpack.ProgressPlugin(),
    ],
    devServer: {
      open: true,
      contentBase: path.resolve(__dirname, 'dist/'),
      watchContentBase: true,
      port: 8080,
      historyApiFallback: {
        index: '/',
      },
      overlay: true,
    },
  };
};
