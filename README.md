# 開発環境構築

## 概要

以下のソフトウェアが必要になる。

- Visutal Studio Code
- Node.js

## Visual Studio Code

### 概要

フロントの開発に用いる。
[公式サイト](https://code.visualstudio.com/)から取得する。

### インストール

公式サイトからインストーラを取得してインストールする。

### 開発環境の構築

web ディレクトリを開く
ファイル > フォルダを開く > `web` を選択

`Ctrl+Shift+X` を押下すると拡張機能のタブが開く。
`@recommended:workspace` で検索して表示された拡張機能を全てインストールする。

リポジトリの `src/web/` を Visual Studio Code で開くと web の開発を行える。

Node.js の実行は `Ctrl+@` で開くターミナルから行う。

## Node.js

### 概要

フロントのビルドやパッケージ管理に使用する。
[公式サイト](https://nodejs.org/ja/)から最新版を取得する。

### インストール

公式サイトからインストーラを取得してインストールする。

### 開発環境の構築

※以下のコマンドは Visual Studio Code のターミナルあるいはコマンドプロンプトから実行する。

Proxy の設定

```bash
npm config set proxy centaro.lis.co.jp:8080
npm config set https-proxy centaro.lis.co.jp:8080
```

開発/運用で使用するツールをインストールする。

```bash
npm install -g @vue/cli
```

開発で使用するパッケージを導入する。

```bash
npm install
```

## Firebase

### 概要

バックエンドとして使用する。

### インストール

コンソール上で以下のコマンドを実行する。

```
npm install -g firebase-tools
```

### 開発サーバ

開発サーバの立ち上げは以下のコマンド。  
画面には http://localhost:8080/ からアクセス可能。

```
npm run serve
```

### デプロイ

ログインを行う。  
「匿名の情報を収集していいか？」と聞いてくるので、Y/N で答える。  
Google アカウントでログインし、権限の承認を行う。  
「Firebase CLI Login Successful」と表示されれば成功。

```
firebase login
```

デプロイして web ページを開く。

```
npm run build
firebase deploy
firebase open hosting:site
```
